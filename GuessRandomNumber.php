<?php
class GuessRandomNumber {

	public function guessNumber($number) {

		$guess 		= $this->randomNumber(1, 10);
		$number 	= (int)$number;
		$difference = abs($guess-$number);
		$message 	= "";
		$attempts 	= $this->attemptSession();
		if($attempts == "-1" || ($attempts == 3 && $difference != 0)) {

			//return error or refresh page
			$message = "You Lost the Game. Try Again";
			$_SESSION['attempts'] = 0;
		} else {

			//main logic 
			if($guess == $number) {

				// Guess is correct
				$message = "Right! You have won the game";
				$_SESSION['attempts'] = 0;

			} else if($difference >= 3) {

				//print cold
				$message = "Cold";

			} else if($difference >= 2) {

				//print warm
				$message = "Warm";

			} else if($difference == 1) {

				//print hot
				$message = "Hot";
			}
			
		}
		echo $this->responseObject($attempts, $message);

		 
	}
	/** 
	* Attempts Session
	**/
	public function attemptSession() {

		if(isset($_SESSION['attempts'])) {

			$_SESSION['attempts'] += 1;
		} else {

			$_SESSION['attempts'] = 1;
		}
		
		if($_SESSION['attempts'] > 3) {

			return "-1";	
		} else {

			return $_SESSION['attempts'];
		}
	}
	/**
	* Response Object
	**/
	public function responseObject($attempts, $message) {

		$result = array('attempts' => $attempts, 'message' => $message);
		return json_encode($result);
	}
	/**
	* Random number generator
	**/
	public function randomNumber($min, $max) {
		
		if(!isset($_SESSION)) { 
	        session_start(); 
	    }
		if(!isset($_SESSION['random'])) {

			$_SESSION['random'] = rand($min, $max);
		} 
		return $_SESSION['random'];
	}

}
//check the number 
if(isset($_POST['number'])) {

	//create object and call the function
	$guessObj = new GuessRandomNumber();
	$guessObj->guessNumber($_POST['number']);
	
} else {

	echo "Invalid Number";
}

?>