<!DOCTYPE html>
<html>
<head>
	<title>Pick Random Number From 1 to 10</title>
	<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous">
		
	</script>
</head>
<body>
	
	<label>I am thinking a number from 1 to 10.</label><br/>
	<label>You must guess what it is in three tries.</label><br/>
	<label>Enter a guess</label>
	<input type="text" name="number" id="number">
	<input type="button" name="guess" value="Guess It" id="guess_it"> <br/>	
	<span id = "message"></span>
	
</body>
</html>
<?php 
session_start();
session_destroy(); 

?>
<script type="text/javascript">

	$(document).ready(function() {
		//check number onclick
		$("#guess_it").click(function() {
			
			$("#message").html("");
			var number = parseInt($("#number").val());

			if(number && number>=1 && number <= 10) {
				//do ajax call to check the number with guess
				$.ajax({
					method: "POST",
					url: "GuessRandomNumber.php",
					type: "JSON",
					data: { "number": number },
					success: function(data) {

						var responseObj = JSON.parse(data);

						var message = responseObj.message;
						var attempt = responseObj.attempts;
						var trials 	= '';
						if(attempt == 1) {
							trials = "first";
						} else if(attempt == 2) {
							trials = "second";
						} else if(attempt == 3) {
							trials = "last";
						} 
						var responseMessage = "Your "+trials+" guess is: "+number;
						if(message == "Right! You have won the game") {
							responseMessage += "<br/>"+message;
						} else {
							responseMessage += " ("+message+")"
						}
						$("#message").html(responseMessage);

						
					}
				});
			} else if(number<1 || number > 10) {
				$("#message").text("Select a number from 1 to 10");
			} else {
				//throw err 
				$("#message").text("Enter valid number");
			}
		});
	});
</script>
